import createError from "http-errors"
import fs from "fs";
import marked from "marked";
import matter from 'gray-matter';
import path from "path";

// TODO parameter that
export const CONTENT_DIR = "content"
export const LIMIT = 20
export const MARKDOWN_EXT = ".md"

export function readMarkdownFiles({ collection, limit = LIMIT }) {
  // TODO add tests, implement limit, add option not to return content for every file
  const dirPath = path.resolve(CONTENT_DIR, collection)
  if (!fs.existsSync(dirPath)) {
    throw createError(404, `Directory ${collection} was not found in content directory`)
  }
  return fs
    .readdirSync(dirPath)
    .filter(file => file[0] !== "." && path.extname(file) === MARKDOWN_EXT)
    .map(file => {
      const slug = path.basename(file, path.extname(file))
      return readMarkdownFile({ collection, slug })
    });
}

export function readMarkdownFile({ collection, slug }) {
  const relativeFilePath = path.join(collection, slug + MARKDOWN_EXT)
  const filePath = path.resolve(CONTENT_DIR, relativeFilePath)
  let fileContent
  try {
    fileContent = fs.readFileSync(filePath, "utf-8");
  } catch (e) {
    console.error(e);
    if (e.code === 'ENOENT') {
      throw createError(404, `File ${relativeFilePath} was not found in content directory`)
    }
    throw createError(500, `File ${relativeFilePath} could not be loaded`)
  }
  const { data: frontMatter, content: md } = matter(fileContent)
  if (frontMatter.slug) {
    throw createError(500, `${relativeFilePath}: "slug" property can't be used in front-matter, the slug is the file name so change the file name`)
  }
  const renderer = new marked.Renderer();
  const html = marked(md, { renderer });
  return { slug, frontMatter, md, html }
}

export default {
  loadCollection: readMarkdownFiles,
  loadPage: readMarkdownFile,
}