import fileSystemLoader from "./filesystem.js"

export const loaders = {
  filesystem: fileSystemLoader
}

// Serve collections and pages from content directory using a loader (e.g. filesystem loader).
export function get(req, res, next) {
  function handleErrors(thunk) {
    let result
    try {
      result = thunk()
    } catch (e) {
      res.writeHead(e.status, { 'Content-Type': 'application/json' });
      return res.end(JSON.stringify({
        error: { message: e.message },
        result: null
      }))
    }
    res.writeHead(200, { 'Content-Type': 'application/json' });
    return res.end(JSON.stringify({ result }));
  }

  // TODO Allow loading from other sources.
  const loader = loaders.filesystem

  const { collection, slug } = req.params

  return !slug || slug === "all"
    ? handleErrors(() => loader.loadCollection({ collection }))
    : handleErrors(() => loader.loadPage({ collection, slug }))
}